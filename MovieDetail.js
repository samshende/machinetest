import React from 'react';
import {
  View,
  Text,
  Container, Header, Content,
  Card,
  Button,
  Icon,
  Spinner
} from 'native-base';
import {
  Image, ScrollView, ImageBackground
} from 'react-native';
import axios from 'react-native-axios'
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
export default class MovieDetails extends React.Component {
  constructor(props) {
    super(props);
    this.setState({
      MovieDetailsArr: {}
    })
  }
  fetch = async () => {
    try {
      const resp = await axios.get(`https://api.themoviedb.org/3/movie/${this.props.navigation.state.params.data.id}?api_key=68e82445c8842ba8616d0f6bf0e97a41`)
      if (resp != null) {
        let data = resp.data != null ? resp.data : {}
        console.log('data det', resp)
        this.setState({
          MovieDetailsArr: data,
        })
      }
    } catch (err) {
      console.error(err);
    }
  }

  componentDidMount() {
    this.fetch()
  }


  render() {
    return (
      <Container>
        <Header style={{ backgroundColor: '#b7babd' }} androidStatusBarColor="grey">
          <Text style={{ color: 'white', fontWeight: 'bold', alignSelf: 'center' }}>Movies Details</Text>
        </Header>

        <ScrollView>
          {this.state && this.state.MovieDetailsArr ?
            <>

              {console.log("img", this.state.MovieDetailsArr.backdrop_path)}
              <Card style={{ height: 'auto', width: wp('98%'), alignSelf: 'center', borderRadius: 10, marginTop: 10 }}>
                {/* <ImageBackground
                style={{ height: "auto", width: '100%' }}
                source={{ uri: `https://image.tmdb.org/t/p/w500${this.state.MovieDetailsArr.backdrop_path}` }}
              > */}

                <View style={{ margin: 10 }}>
                  <View style={{ justifyContent: 'space-between', alignSelf: 'center', flexDirection: 'row', width: wp('70%'), marginBottom: 5 }}>
                    <Image
                      style={{ resizeMode: 'cover', width: 100, height: 100, alignSelf: 'center' }}
                      source={{ uri: `https://image.tmdb.org/t/p/w500${this.state.MovieDetailsArr.backdrop_path}` }}
                    />
                    <Image
                      style={{ resizeMode: 'cover', width: 100, height: 100, alignSelf: 'center' }}
                      source={{ uri: `https://image.tmdb.org/t/p/w500${this.state.MovieDetailsArr.poster_path}` }}
                    />
                  </View>
                  <Text style={{ fontWeight: 'bold', color: 'grey', fontSize: 18 }}>
                    Title                :
              <Text style={{ color: 'grey', fontWeight: '800', fontSize: 16 }}> {this.state.MovieDetailsArr.title}</Text>
                  </Text>

                  <Text style={{ fontWeight: 'bold', color: 'grey', fontSize: 18 }}>
                    Language       :
              <Text style={{ color: 'grey', fontWeight: '800', fontSize: 16 }}> {this.state.MovieDetailsArr.original_language}</Text>
                  </Text>
                  <Text style={{ fontWeight: 'bold', color: 'grey', fontSize: 18 }}>
                    Popularity      :
              <Text style={{ color: 'grey', fontWeight: '800', fontSize: 16 }}> {this.state.MovieDetailsArr.popularity}</Text>
                  </Text>
                  <Text style={{ fontWeight: 'bold', color: 'grey', fontSize: 18 }}>
                    Release date  :
              <Text style={{ color: 'grey', fontWeight: '800', fontSize: 16 }}> {this.state.MovieDetailsArr.release_date}</Text>
                  </Text>
                  <Text style={{ fontWeight: 'bold', color: 'grey', fontSize: 18 }}>
                    Revenue          :
              <Text style={{ color: 'grey', fontWeight: '800', fontSize: 16 }}> {this.state.MovieDetailsArr.revenue}</Text>
                  </Text>
                  <Text style={{ fontWeight: 'bold', color: 'grey', fontSize: 18 }}>
                    Description     :
              <Text style={{ color: 'grey', fontWeight: '800', fontSize: 16 }}> {this.state.MovieDetailsArr.overview}</Text>
                  </Text>
                </View>
                {/* </ImageBackground> */}

              </Card>
            </>
            :
            <Spinner style={{ flex: 1, alignSelf: 'center' }} color='green' />
          }
        </ScrollView>
      </Container>
    )
  }


}