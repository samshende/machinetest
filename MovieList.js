import React from 'react';
import {
  View,
  Text,
  Container, Header,
  Card,
  Button,
  Spinner
} from 'native-base';

import {
  Image,
  ScrollView,
  TouchableWithoutFeedback
} from 'react-native';
import axios from 'react-native-axios'
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';


class MovieList extends React.Component {

  constructor(props) {
    super(props);
    this.setState({
      list: [],
      genId: null,
      MovieList: []
    })
  }


  fetch = async () => {
    try {
      const resp = await axios.get("https://api.themoviedb.org/3/genre/movie/list?api_key=68e82445c8842ba8616d0f6bf0e97a41")
      if (resp != null) {
        let data = resp.data != null && resp.data.genres != null ? resp.data.genres : []
        // console.log('data', data)
        this.fetchMovies(data)
      }
    } catch (err) {
      console.error(err);
    }



  }

  fetchMovies = async (data) => {
    let list = data
    let data11 = []
    for (let i = 0; i < list.length; i++) {
      try {
        const resp = await axios.get(`https://api.themoviedb.org/3/genre/${list[i].id}/movies?api_key=68e82445c8842ba8616d0f6bf0e97a41`)
        if (resp != null) {
          let data = resp.data != null && resp.data.results != null ? resp.data.results : []
          data11.push(data)
        }
      } catch (err) {
        console.error(err);
      }
    }
    // console.log("data11", data11)
    this.setState({
      list: data,
      MovieList: data11
    })

  }

  componentDidMount() {
    this.fetch()
  }

  render() {
    console.log(this.state)
    return (
      <Container>
        <Header style={{ backgroundColor: '#b7babd' }} androidStatusBarColor="grey">
          <Text style={{ color: 'white', fontWeight: 'bold', alignSelf: 'center' }}>Movie List</Text>
        </Header>

        {this.state == null &&
          <Spinner style={{ flex: 1, alignSelf: 'center' }} color='green' />

        }

        <ScrollView>
          <View style={{ marginTop: 10 }}>
            {this.state && this.state.list && this.state.list.length > 0 && this.state.list.map((i, j) => {
              return (
                <Card style={{ height: 'auto', width: wp('98%'), alignSelf: 'center', borderRadius: 10 }}>
                  <Text style={{ fontSize: 18, color: '#e38242', fontWeight: 'bold', textAlign: 'center', marginTop: 10, marginBottom: 10 }}>
                    {i.name ? i.name : '--'}
                  </Text>
                  <ScrollView
                    horizontal={true}
                    showsHorizontalScrollIndicator={false}>
                    {
                      this.state.MovieList[j].map((k, l) => {
                        return (
                          <TouchableWithoutFeedback
                            onPress={
                              () => {
                                this.props.navigation.navigate('MovieDetails', {
                                  data: k
                                });
                              }
                            }
                          >
                            <View style={{ margin: 5 }}>
                              <Image
                                style={{ resizeMode: 'repeat', width:100, height:100,alignSelf:'center' }}
                                source={{ uri: `https://image.tmdb.org/t/p/w500${k.poster_path}` }}
                              />
                              <Text style={{ textAlign: 'center', width: 120,color:'grey' }}>{k.title}</Text>
                            </View>
                          </TouchableWithoutFeedback>
                        )
                      })
                    }
                  </ScrollView>
                </Card>
              )
            })}
          </View>
        </ScrollView>
      </Container>
    )
  }
}

export default MovieList;