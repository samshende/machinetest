
import React from 'react';
import {createStackNavigator} from 'react-navigation'
import MovieDetails from './MovieDetail'
import MovieList from './MovieList'
import {
    Root
  } from 'native-base';

const Navigator=createStackNavigator(

    {
        MovieDetails:{
            screen:MovieDetails,navigationOptions:{
                header:null
            },
        },
        MovieList:{
            screen:MovieList,navigationOptions:{
                header:null
            },
        },
    },

    {
        initialRouteName:'MovieList',
        headerMode:'none',
        mode:'Model'
    }
)


const App=(props)=>(
    <Root>
        <Navigator/>
    </Root>
)

export default App;